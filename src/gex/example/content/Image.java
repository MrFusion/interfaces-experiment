package gex.example.content;

import gex.example.interfaces.CoverItem;
import gex.example.interfaces.Media;

/**
 * An image with full metadata
 */
public class Image extends Content implements CoverItem, Media {

  StorageItem mainImage;

  public StorageItem getMainImage() {
    return mainImage;
  }

  @Override
  public StorageItem getCoverImage() {
    return mainImage;
  }
}
