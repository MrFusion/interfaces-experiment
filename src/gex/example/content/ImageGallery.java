package gex.example.content;

import gex.example.interfaces.CoverItem;
import gex.example.interfaces.Media;
import gex.example.interfaces.Thumbnailable;

import java.util.List;

public class ImageGallery extends Content implements Media, CoverItem, Thumbnailable {

  List<Slide> slides;

  @Override
  public StorageItem getCoverImage() {
    return slides.stream().findFirst().map(Slide::getCoverImage).orElse(null);
  }

  static class Slide implements Thumbnailable {

    Image image;
    String headline;
    String description;

    @Override
    public StorageItem getCoverImage() {
      return image.getCoverImage();
    }
  }

}
