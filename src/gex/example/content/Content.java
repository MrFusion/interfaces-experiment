package gex.example.content;

public abstract class Content {

  String headline;
  String description;
  String permalink;

  public String getHeadline() {
    return headline;
  }

  public void setHeadline(String headline) {
    this.headline = headline;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getPermalink() {
    return permalink;
  }

  public void setPermalink(String permalink) {
    this.permalink = permalink;
  }

  public String getCoverHeadline() {
    return headline;
  }

  public String getCoverDescription() {
    return description;
  }

}
