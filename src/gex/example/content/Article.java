package gex.example.content;

import gex.example.interfaces.CoverItem;
import gex.example.interfaces.Media;

public class Article extends Content implements CoverItem, Media {

  Media mainMedia;
  StorageItem coverImage;

  @Override
  public StorageItem getCoverImage() {
    return coverImage;
  }

}
