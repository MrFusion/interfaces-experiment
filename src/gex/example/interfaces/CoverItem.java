package gex.example.interfaces;

/**
 * Content that can be shown on homepages and any dynamic listing
 */
public interface CoverItem extends Thumbnailable {

  public String getCoverHeadline();
  public String getCoverDescription();
  public String getPermalink();

}
