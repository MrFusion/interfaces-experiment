package gex.example.interfaces;

import gex.example.content.StorageItem;

/**
 * Represents a content that has a thumbnail to show
 */
public interface Thumbnailable {

  public StorageItem getCoverImage();

}
