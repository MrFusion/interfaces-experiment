package gex.example.interfaces;

/**
 * Marker interface for any content that can be selected as main media for Articles
 */
public interface Media extends Thumbnailable {

}
