package gex.example.layout;

import gex.example.content.Article;
import gex.example.content.ImageGallery;
import gex.example.interfaces.CoverItem;

import java.util.Arrays;
import java.util.List;

public class Homepage {

  public static void main() {
    //Example
    List<CoverItem> contentList = Arrays.asList(
        new CustomCoverItem(),
        new ImageGallery(),
        new Article()
    );

  }

  public void renderContentList(List<CoverItem> items) {

    items.stream()
      .forEach(coverItem -> {

        //Render everything
        coverItem.getCoverHeadline();
        coverItem.getCoverDescription();
        coverItem.getCoverImage();

      });

  }

}
