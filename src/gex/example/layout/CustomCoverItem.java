package gex.example.layout;

import gex.example.content.StorageItem;
import gex.example.interfaces.CoverItem;

public class CustomCoverItem implements CoverItem {

  CoverItem content;
  String headline;
  String description;
  StorageItem coverImage;

  @Override
  public String getCoverHeadline() {
    return headline != null ? headline : content.getCoverHeadline();
  }

  @Override
  public String getCoverDescription() {
    return description != null ? description : content.getCoverDescription();
  }

  @Override
  public String getPermalink() {
    return content.getPermalink();
  }

  @Override
  public StorageItem getCoverImage() {
    return coverImage != null ? coverImage : content.getCoverImage();
  }
}
